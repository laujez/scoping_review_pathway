library(stringr)
library(tidytext)
library(dplyr)
library(ggplot2)

setwd("D:/laura/Documents/stage_data/scoping_review_pathway")

#type_of_graphical_representation
review <- read.csv("review.csv",
                   sep=",",
                   header = TRUE)
type_of_graphical_representation=review[,"type_of_graphical_representation"]
review <- as.data.frame(cbind(index=1:30,type_of_graphical_representation=review[,"type_of_graphical_representation"]))

ReviewWords <- review %>%
                    unnest_tokens(output=type_of_graphical_representation,input="type_of_graphical_representation")%>% 
                    count(index,type_of_graphical_representation)

barplot <- ggplot(data=ReviewWords, aes(x=type_of_graphical_representation, y=n , fill=type_of_graphical_representation))+geom_bar(stat="identity")
barplot

#techno
review <- read.csv("review.csv",
                   sep=",",
                   header = TRUE)
type_of_graphical_representation=review[,"type_of_graphical_representation"]
review <- as.data.frame(cbind(index=1:30,techno=review[,"techno"]))

ReviewWords <- review %>%
  unnest_tokens(output=techno,input="techno")%>% 
  count(index,techno)

barplot <- ggplot(data=ReviewWords, aes(x=techno, y=n , fill=techno))+geom_bar(stat="identity")
barplot

#event_type
review <- read.csv("review.csv",
                   sep=",",
                   header = TRUE)
type_of_graphical_representation=review[,"type_of_graphical_representation"]
review <- as.data.frame(cbind(index=1:30,Event.type=review[,"Event.type"]))

ReviewWords <- review %>%
  unnest_tokens(output=Event.type,input="Event.type")%>% 
  count(index,Event.type)

barplot <- ggplot(data=ReviewWords, aes(x=Event.type, y=n , fill=Event.type))+geom_bar(stat="identity")
barplot
